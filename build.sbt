import Dependencies._

name := "bbs_backend_team"

version := "1.0"

lazy val commonSettings = Seq(
  libraryDependencies ++= Seq(evolutions, jdbc, ehcache, ws, filters, specs2 % Test, guice, jwtPlay, jwtCore, jwtRsa, bcrypt, makeCsv,mail,mailguide),
  scalaVersion := "2.13.3",
  Compile / scalaSource := baseDirectory.value / "src" / "main" / "scala",
  Test / scalaSource := baseDirectory.value / "src" / "test" / "scala",
  Compile / resourceDirectory := baseDirectory.value / "src" / "main" / "resources",
  Test / resourceDirectory := baseDirectory.value / "src" / "test" / "resources"
)

lazy val root = (project in file("."))
  .enablePlugins(PlayScala)
  .disablePlugins(PlayLayoutPlugin)
  .aggregate(application, domain, port, utility)
  .dependsOn(utility, domain, port)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(filters, guice, scalaGuice))
  .settings(routesGenerator := InjectedRoutesGenerator)

lazy val application = (project in file("app/application"))
  .enablePlugins(PlayScala)
  .disablePlugins(PlayLayoutPlugin)
  .dependsOn(utility, domain)
  .settings(commonSettings: _*)

lazy val domain = (project in file("app/domain"))
  .enablePlugins(PlayScala)
  .disablePlugins(PlayLayoutPlugin)
  .dependsOn(utility)
  .settings(commonSettings: _*)

lazy val utility = (project in file("app/utility"))
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= portUtilityDependencies)

lazy val port = (project in file("app/port"))
  .aggregate(portWebService, portDatabase, portFile, portHttp)
  .dependsOn(portWebService, portDatabase, portFile, portHttp)
  .settings(commonSettings: _*)

lazy val portWebService = (project in file("app/port/primary/webService"))
  .enablePlugins(PlayScala, SbtWeb)
  .disablePlugins(PlayLayoutPlugin)
  .dependsOn(utility, application)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(tika))

lazy val portDatabase = (project in file("app/port/secondary/databaseMySQL"))
  .dependsOn(utility, application)
  .settings(libraryDependencies ++= portDatabaseDependencies)
  .settings(commonSettings: _*)

lazy val portFile = (project in file("app/port/secondary/file"))
  .dependsOn(utility, application)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(tika))

lazy val portHttp = (project in file("app/port/secondary/http"))
  .enablePlugins(PlayScala)
  .disablePlugins(PlayLayoutPlugin)
  .dependsOn(utility, application)
  .settings(commonSettings: _*)
  .settings(libraryDependencies ++= Seq(ws))
