package image

import Services.ImageService
import org.apache.tika.Tika
import play.api.libs.Files
import play.api.mvc.MultipartFormData

import java.io.File
import java.nio.file.Path
import java.util.UUID
import scala.util.Try

class ImageHandle extends ImageService{
  override def load(imageName: String): Try[(Path, String)] = {
    Try {
      val tika = new Tika()
      val file = new File(s"src/main/resources/images/$imageName")
      (file.toPath, tika.detect(file))
    }
  }

  override def upload(image: MultipartFormData.FilePart[Files.TemporaryFile]): Try[(Path, String)] = {
    Try {
        val imageName = UUID.randomUUID().toString
        val path = image.ref.copyTo(new File(s"src/main/resources/images/$imageName"))
        (path, imageName)
    }
  }
}
