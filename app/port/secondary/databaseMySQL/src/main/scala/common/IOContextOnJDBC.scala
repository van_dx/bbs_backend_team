package common

import scalikejdbc.DBSession
import repository.IOContext
case class IOContextOnJDBC(session: DBSession) extends IOContext