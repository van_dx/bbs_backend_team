package common

import skinny.orm.SkinnyCRUDMapperWithId

trait CRUDMapperWithId[ID, ObjectFields, Entity] extends CRUDMapper[ObjectFields, Entity]
  with SkinnyCRUDMapperWithId[ID, Entity] {
}