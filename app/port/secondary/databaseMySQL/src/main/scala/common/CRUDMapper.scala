package common

import skinny.orm.SkinnyMapperBase

trait CRUDMapper[ObjectFields, Entity] extends SkinnyMapperBase[Entity] {
  def toNamedValues(entity: Entity): Seq[(Symbol, Any)]

}