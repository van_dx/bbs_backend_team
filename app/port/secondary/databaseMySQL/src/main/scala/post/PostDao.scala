package post

import common.CRUDMapperWithId
import post.model.PostFields
import scalikejdbc.WrappedResultSet
import skinny.orm.Alias

private[post]
class PostDao extends CRUDMapperWithId[Long,PostFields,PostRecord]{
  override lazy val defaultAlias: Alias[PostRecord] = createAlias("post")
  override lazy val tableName = "post"
  override lazy val primaryKeyFieldName = "id"

  override def columnNames: Seq[String] = Seq(
    "id",
    "category",
    "title",
    "content",
    "author_name",
    "created_date",
    "updated_date",
    "thumbnail",
    "id_user"
  )
def mutiple (a:Int,b:Int):Int = a*b
  override def idToRawValue(id: Long): Long = id

  override def rawValueToId(value: Any): Long = value.asInstanceOf[Long]

  override def toNamedValues(entity: PostRecord): Seq[(Symbol, Any)] = Seq(
    Symbol("id") -> entity.id,
    Symbol("category") -> entity.category,
    Symbol("title") -> entity.title,
    Symbol("content") -> entity.content,
    Symbol("author_name") -> entity.authorName,
    Symbol("created_date") -> entity.createdDate,
    Symbol("updated_date") -> entity.updatedDate,
    Symbol("thumbnail") -> entity.thumbnail,
    Symbol("id_user") -> entity.idUser
  )

  override def extract(rs: WrappedResultSet, n: scalikejdbc.ResultName[PostRecord]): PostRecord =
    PostRecord(
      id = rs.get(n.id),
      category = rs.get(n.category),
      title = rs.get(n.title),
      content = rs.get(n.content),
      authorName = rs.get(n.authorName),
      createdDate = rs.get(n.createdDate),
      updatedDate = rs.get(n.updatedDate),
      thumbnail = rs.get(n.thumbnail),
      idUser = rs.get(n.idUser)
    )

}
