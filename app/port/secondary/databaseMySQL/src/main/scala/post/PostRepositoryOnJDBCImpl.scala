package post

import common.BasicWithIdFeatureRepositoryOnJDBC
import exeption.EntityNotFoundException
import post.model._
import repository.IOContext
import scalikejdbc.jodatime.JodaBinders
import scalikejdbc.{DB, TypeBinder, WrappedResultSet, scalikejdbcSQLInterpolationImplicitDef, sqls}
import user.model.UserId

import scala.util.Try

class PostRepositoryOnJDBCImpl(override protected[this] val dao: PostDao = new PostDao) extends PostRepository with BasicWithIdFeatureRepositoryOnJDBC[PostId, PostFields, Post, Long, PostRecord] {
  override type DAO = PostDao

  override protected def fieldsFromNamedValues(fields: PostFields): Seq[(Symbol, Any)] = Seq(
    Symbol("category") -> fields.category,
    Symbol("title") -> fields.title,
    Symbol("content") -> fields.content,
    Symbol("author_name") -> fields.authorName,
    Symbol("created_date") -> fields.createdDate,
    Symbol("updated_date") -> fields.updatedDate,
    Symbol("thumbnail") -> fields.thumbnail,
    Symbol("id_user") -> fields.idUser
  )

  override def convertToIdentifier(id: Long): PostId = PostId(id)

  override protected def convertToEntity(record: PostRecord): Post = {
    Post(
      identifier = PostId(record.id),
      category = record.category,
      title = record.title,
      content = record.content,
      authorName = record.authorName,
      createdDate = record.createdDate,
      updatedDate = record.updatedDate,
      thumbnail = record.thumbnail,
      idUser = UserId(record.idUser)
    )

  }

  override def findByUserId(id: Long): Try[Seq[Post]] = withDBSession {
    implicit session =>
      dao.findAllBy(sqls.eq(dao.column.idUser, id)).map(convertToEntity)
  }

  override def createPost(post: Post): Try[Long] = withDBSession {
    implicit session => store(post).getOrElse(throw new Exception(s"models = ${dao.tableName}")).value
  }

  override def findAllActive(): Try[Seq[Post]] = withDBSession {
    implicit session =>
      dao.findAllWithLimitOffset(
        orderings = Seq(sqls"id desc")
      ).map(convertToEntity)
  }

  override protected def convertToRecordId(identifier: PostId): Long = identifier.value

  override def findById(id: Long): Try[Post] = withDBSession {
    implicit session =>
      resolveBy(PostId(id)).getOrElse(throw new EntityNotFoundException(s"models = ${dao.tableName}, id = $id"))
  }

  override def getNumOfPosts: Try[Int] = ???

  override def updatePost(post: Post): Try[Unit] = withDBSession {
    implicit session => super.update(post).getOrElse(throw new EntityNotFoundException(s"models = ${dao.tableName}"))
  }

  override def deletePost(id: Long): Try[Long] = withDBSession {
    implicit session => dao.deleteById(id)
  }

  override def delete(id: PostId)(implicit ctx: IOContext): Try[Unit] = ???

  implicit val time: TypeBinder[org.joda.time.DateTime] = JodaBinders.jodaDateTime

  def mappingData(rs: WrappedResultSet): Post = {
    Post(
      identifier = PostId(rs.get("id")),
      category = rs.get("category"),
      title = rs.get("title"),
      content = rs.get("content"),
      authorName = rs.get("author_name"),
      createdDate = Some(rs.get("created_date")),
      updatedDate = Some(rs.get("updated_date")),
      thumbnail = rs.get("thumbnail"),
      idUser = UserId(rs.get("id_user"))

    )

  }

  override def findByName(value: String): Try[Seq[Post]] = DB readOnly { implicit session =>
    Try {
      sql"""SELECT
          *
         FROM post AS p
         WHERE p.category = ${value} or p.title = ${value} or p.author_name = ${value} GROUP BY p.id"""
        .map { rs => mappingData(rs) }
        .list().apply()
    }

  }
}
