package post

import modelutil.Record
import org.joda.time.DateTime
case class PostRecord (
                       id: Long,
                       category:String,
                       title: String,
                       content: String,
                       authorName: String,
                       override val createdDate: Option[DateTime],
                       override val updatedDate: Option[DateTime],
                       thumbnail: String,
                       idUser: Long
                      ) extends Record


