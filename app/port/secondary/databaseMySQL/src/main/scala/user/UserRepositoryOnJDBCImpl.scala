package user

import common.BasicWithIdFeatureRepositoryOnJDBC
import exeption.{EmailAlreadyExist, EntityNotFoundException}
import repository.IOContext
import scalikejdbc.sqls
import user.model.{User, UserFields, UserId, UserImpl}

import scala.util.{Failure, Success, Try}

class UserRepositoryOnJDBCImpl(override protected[this] val dao: UserDao = new UserDao)
  extends UserRepository with BasicWithIdFeatureRepositoryOnJDBC[UserId, UserFields, User, Long, UserRecord] {

  override type DAO = UserDao

  override def findByEmail(email: String): Try[User] = withDBSession(
    implicit session =>
      dao.findBy(sqls.eq(dao.column.email, email)).map(convertToEntity)
        .getOrElse(throw new EntityNotFoundException(s"Enitty not found at ${dao.tableName}, email = ${email}"))
  )

  override protected def fieldsFromNamedValues(fields: UserFields): Seq[(Symbol, Any)] = Seq(
    Symbol("email") -> fields.email,
    Symbol("password") -> fields.password,
    Symbol("username") -> fields.username,
    Symbol("enable") -> fields.enable,
    Symbol("createdDate") -> fields.createdDate,
    Symbol("updatedDate") -> fields.updatedDate
  )

  override def convertToIdentifier(id: Long): UserId = UserId(id)

  override protected def convertToEntity(record: UserRecord): User = {
    User(
      id = UserId(record.id),
      email = record.email,
      password = record.password,
      username = record.username,
      enable = record.enable,
      createdDate = record.createdDate,
      updatedDate = record.updatedDate
    )
  }

  override protected def convertToRecordId(identifier: UserId): Long = identifier.value


  override def delete(id: UserId)(implicit ctx: IOContext): Try[Unit] = ???

  override def saveNewUser(newUser: UserImpl): Try[Long] = withDBSession{
    implicit session =>
      findByEmail(newUser.email) match {
        case Success(_) => throw new EmailAlreadyExist(s"${newUser.email} is already exist")
        case Failure(_) => store(newUser).getOrElse(throw new Exception(s"Error! Can't store entity, models = ${dao.tableName}")).value
      }
  }
}
