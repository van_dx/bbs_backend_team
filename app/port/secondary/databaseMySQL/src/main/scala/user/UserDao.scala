package user

import common.CRUDMapperWithId
import scalikejdbc.WrappedResultSet
import skinny.orm.Alias
import user.model.UserFields

class UserDao extends CRUDMapperWithId[Long, UserFields ,UserRecord]{
  override lazy val defaultAlias: Alias[UserRecord] = createAlias("users")
  override lazy val tableName = "users"
  override lazy val primaryKeyFieldName = "id"

  override def toNamedValues(entity: UserRecord): Seq[(Symbol, Any)] = Seq(
    Symbol("id") -> entity.id,
    Symbol("email") -> entity.email,
    Symbol("password") -> entity.password,
    Symbol("username") -> entity.username,
    Symbol("enable") -> entity.enable,
    Symbol("createdDate") -> entity.createdDate,
    Symbol("updatedDate")  -> entity.updatedDate
  )

  override def idToRawValue(id: Long): Any = id

  override def rawValueToId(value: Any): Long = value.asInstanceOf[Long]

  override def extract(rs: WrappedResultSet, n: scalikejdbc.ResultName[UserRecord]): UserRecord = {
    UserRecord(
      rs.get(n.id),
      rs.get(n.email),
      rs.get(n.password),
      rs.get(n.username),
      rs.get(n.enable),
      rs.get(n.createdDate),
      rs.get(n.updatedDate)

    )
  }
}
