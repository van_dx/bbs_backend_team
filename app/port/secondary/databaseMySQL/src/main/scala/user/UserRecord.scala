package user

import modelutil.Record
import org.joda.time.DateTime

case class UserRecord(id: Long, email: String, password: String, username: String,enable: Boolean, override val createdDate: Option[DateTime], override val updatedDate: Option[DateTime]) extends Record
