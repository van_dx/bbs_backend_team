package dto.post

import post.model.Post


case class PostJson(
  id: Option[Long],
  category: String,
  title: String,
  content: String,
  authorName: String,
  createdDate: String,
  updatedDate: String,
  thumbnail: String,
  idUser: Option[Long])

object PostResponse {
  def apply(posts: Seq[Post]): Seq[PostJson] = posts.map(PostResponse(_))

  def apply(post: Post): PostJson = PostJson(
    id = Some(post.identifier.value),
    category = post.category,
    title = post.title,
    content = post.content,
    authorName = post.authorName,
    createdDate = post.createdDate.mkString,
    updatedDate = post.updatedDate.mkString,
    thumbnail = post.thumbnail,
    idUser = Some(post.idUser))
}