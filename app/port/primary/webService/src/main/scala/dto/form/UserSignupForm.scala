package dto.form

import org.joda.time.DateTime
import play.api.data.Forms._
import play.api.data.validation.{Constraint, Invalid, Valid, ValidationError}
import play.api.data.{Form, Mapping}
import user.model.{User, UserId}


object UserSignupForm {
  //  Password must have at least 8 characters including letters, numbers and special characters?
  val PASSWORD_CONSTRAINT_REG = """^(?=.*[A-Za-z])(?=.*\d)(?=.*[@$!%*#?&])[A-Za-z\d@$!%*#?&]{8,}$""".r
  val passwordCheckConstraint: Constraint[String] = Constraint({
    password =>
      val errors = password match {
        case PASSWORD_CONSTRAINT_REG() => Nil
        case _ => Seq(ValidationError("password is not valid"))
      }
      if (errors.isEmpty) {
        Valid
      } else {
        Invalid(errors)
      }
  })

  val form: Form[User] = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText(minLength = 8, maxLength = 20).verifying(passwordCheckConstraint),
      "username" -> nonEmptyText(minLength = 1, maxLength = 50)
    )(User.apply)(User.unapply)
  )
}
