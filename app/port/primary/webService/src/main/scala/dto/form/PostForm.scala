package dto.form

import org.joda.time.DateTime
import play.api.data.Forms.{longNumber, mapping, nonEmptyText, optional}
import play.api.data.{Form, Mapping}
import post.model.{Post, PostId}
import user.model.UserId


object PostForm {
  val dateTimeMapping: Mapping[DateTime] = nonEmptyText.transform(DateTime.parse, (date: DateTime) => date.toString("yyyy-mm-dd"))

  val idMapping: Mapping[PostId] =
    optional(longNumber).transform((id: Option[Long]) => PostId(id.getOrElse(0)), (id: PostId) => Some(id.value))

  val idMappingUser: Mapping[UserId] =
    optional(longNumber).transform((id: Option[Long]) => UserId(id.getOrElse(0)), (id: UserId) => Some(id.value))
  val form: Form[Post] = Form(
    mapping(
      "id" -> idMapping,
      "category" -> nonEmptyText(maxLength = 200,minLength = 1),
      "title" -> nonEmptyText(maxLength = 200, minLength = 1),
      "content" -> nonEmptyText(maxLength = 5000, minLength = 1),
      "authorName" -> nonEmptyText(maxLength = 50, minLength = 1),
      "createdDate" -> optional(dateTimeMapping),
      "updatedDate" -> optional(dateTimeMapping),
      "thumbnail" -> nonEmptyText,
     "idUser" -> idMappingUser)
    (Post.apply)(Post.unapply))

}
