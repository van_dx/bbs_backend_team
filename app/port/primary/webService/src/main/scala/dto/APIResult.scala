package dto

case class WarningParameter(key: String, code: String, message: String)

case class ErrorParameter(code: Int, message: String)

case class APIResult(
                      success: Boolean,
                      result: Option[Any] = None,
                      warnings: Option[Seq[WarningParameter]] = None,
                      error: Option[ErrorParameter] = None)

object APIResult {
  def toSuccessJson(result: Any): APIResult = {
    APIResult(success = true, Some(result))
  }

  def toWarningJson(warnings: Seq[WarningParameter]): APIResult = {
    APIResult(success = false, None, Some(warnings), None)
  }

  def toErrorJson(code: Int, message: String): APIResult = {
    APIResult(success = false, None, None, Some(ErrorParameter(code, message)))
  }

}