package dto.user

import user.model.User


case class UserJson (
  id: Option[Long],
  email: String,
  password: String,
  username: String,
  enable: Boolean)

object UserResponse{
  def apply(users: Seq[User]): Seq[UserJson] = users.map(UserResponse(_))

  def apply(user: User): UserJson = UserJson(
    id = Some(user.identifier.value),
    email = user.email,
    password = user.password,
    username = user.username,
    enable = user.enable
  )
}
