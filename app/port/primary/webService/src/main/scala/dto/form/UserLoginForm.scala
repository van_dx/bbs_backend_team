package dto.form

import play.api.data.Form
import play.api.data.Forms.{email, mapping, nonEmptyText}
import user.model.User


object UserLoginForm {
  val form: Form[User] = Form(
    mapping(
      "email" -> email,
      "password" -> nonEmptyText(maxLength = 50)
    )(User.apply)(User.unapply)
  )
}
