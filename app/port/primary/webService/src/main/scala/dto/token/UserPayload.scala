package dto.token

import common.JwtUtility
import exeption.BaseException
import org.joda.time.DateTime
import play.api.libs.json.Json.toJson
import play.api.libs.json._
import user.model.{User, UserImpl}

import scala.util.{Failure, Try}


case class UserPayload (
                         id: Long,
                         email: String,
                         username: String,
                         createAt: Long = DateTime.now.getMillis)

object UserPayload{
  implicit val format: OFormat[UserPayload] = Json.format[UserPayload]

  def encode(user: User): String = {
    JwtUtility.generateToken(
      toJson(UserPayload(user.identifier.value, user.email, user.username)).toString)
  }
  def decode(token:String):Try[UserPayload] = {
      Try(Json.parse(JwtUtility.decodePayload(token).get).validate[UserPayload].get)
  }
}
