package controller.authentication

import Services.{AuthenticationService, SendMailService, UserService}
import controller.APIController
import dto.form.{UserLoginForm, UserSignupForm}
import dto.token.UserPayload
import exeption.{EntityNotFoundException, PasswordNotMatch}
import org.joda.time.DateTime
import org.mindrot.jbcrypt.BCrypt
import play.api.Configuration
import play.api.libs.json.JsValue
import play.api.mvc.{Action, AnyContent, ControllerComponents}
import user.model.{User, UserId, UserImpl}

import javax.inject.{Inject, Singleton}
import scala.util.{Failure, Success, Try}

@Singleton
class AuthenticationController @Inject()(
                                          cc: ControllerComponents,
                                          authenticationService: AuthenticationService,
                                          authenticator: Authenticator,
                                          userService: UserService,
                                          mailService: SendMailService,
                                          conf: Configuration) extends APIController(cc) {

  def login: Action[JsValue] = Action(parse.json) {
    implicit request =>
      UserLoginForm.form.bindFromRequest().fold(
        formWithError => badRequestWarning(formWithError.errors),
        success = user => {
          authenticationService.verify(user) match {
            case Success(result) => success(UserPayload.encode(result))
            case Failure(exception) => exception match {
              case _: PasswordNotMatch => error(Unauthorized, 401, "Password not match or Account is not activated yet, please check the activation email!")
              case _: EntityNotFoundException => error(BadRequest, 400, "Email does not exist")
              case _ => error(InternalServerError, 500, "An error occurred")
            }
          }
        }
      )
  }

  def signUp: Action[AnyContent] = Action { implicit request =>
    UserSignupForm.form.bindFromRequest().fold(
      formWithError => badRequestWarning(formWithError.errors),
      user => {
        val userCurrent = UserImpl(UserId(0), user.email, BCrypt.hashpw(user.password, BCrypt.gensalt()), user.username, false, Some(new DateTime()), Some(new DateTime()))
        val id = userService.create(userCurrent)
        id match {
          case Success(_) => success(mailService.sendEmail(user.email, UserPayload.encode(userCurrent)))
          case _ => error(InternalServerError, 500, "Error! Email already exists!")
        }
      }
    )
  }

  def activeAccount(token: String): Action[AnyContent] = Action {
    val result = for {
      userPayload <- UserPayload.decode(token)
      user <- userService.findByEmail(userPayload.email)
      userActive <- Try(User.apply(user))
      actived <- userService.update(userActive)
    } yield actived
    result match {
      case Success(()) => success("Active success!")
      case _ => error(BadRequest, 400, "The token is expired time!")
    }
  }

  def check(): Action[AnyContent] = authenticator.JwtAuthentication {
    success("Token is valid")
  }

}
