package controller.file

import Services.ImageService
import akka.stream.scaladsl.FileIO
import controller.APIController
import controller.authentication.Authenticator
import dto.WarningParameter
import play.api.libs.Files
import play.api.mvc.{Action, AnyContent, ControllerComponents, MultipartFormData}

import javax.inject.Inject
import scala.util.{Failure, Success}

class ImageController @Inject()(imageService: ImageService, authenticator: Authenticator, cc: ControllerComponents) extends APIController(cc) {
  def upload(): Action[MultipartFormData[Files.TemporaryFile]] = authenticator.JwtAuthentication(parse.multipartFormData) {
    implicit request =>
      request.body.file("thumbnail").fold(error(InternalServerError, 500, "Can not upload image!")) {
        image =>
          val imageType = image.contentType.getOrElse("")
          imageType match {
            case s if s.matches("""image/png|image/jpeg|image/jpg""") => val result = imageService.upload(image)
              result match {
                case Success(result) => success(result._2)
                case Failure(exception) => badRequestWarning(WarningParameter("warn.uploadFail", "warn.uploadFail", exception.getMessage))
              }
            case _ => badRequestWarning(WarningParameter("warn.typeNotMatch", "warn.typeNotMatch", "Only accept image file"))
          }
      }
  }

  def load(imageName: String): Action[AnyContent] = Action {
    val path = imageService.load(imageName)
    path match {
      case Success(result) => successFile(FileIO.fromPath(result._1), result._2)
      case Failure(err) => badRequestWarning(WarningParameter("warn.loadImageFail", "warn.loadImageFail", err.getMessage))
    }
  }
}
