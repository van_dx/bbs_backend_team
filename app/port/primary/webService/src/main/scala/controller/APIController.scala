package controller

import akka.stream.IOResult
import akka.stream.scaladsl.Source
import akka.util.ByteString
import dto.{APIResult, WarningParameter}
import org.json4s.DefaultFormats
import org.json4s.jackson.Serialization.write
import play.api.data.FormError
import play.api.http.HttpEntity
import play.api.mvc.{AbstractController, ControllerComponents, ResponseHeader, Result}

import scala.concurrent.Future

abstract class APIController(cc: ControllerComponents) extends AbstractController(cc){

  implicit val formats = DefaultFormats

  protected def success[T](result: T): Result = Ok(write(APIResult.toSuccessJson(result)))

  protected def success[T](result: Seq[T]): Result = Ok(write(APIResult.toSuccessJson(result)))

  protected def successFile(source: Source[ByteString, Future[IOResult]], contentType: String): Result =
    Result(
      header = ResponseHeader(200, Map.empty),
      body = HttpEntity.Streamed(source, None, Some(contentType)))

  protected def badRequestWarning(formErrors: Seq[FormError]): Result = {
    BadRequest(write(APIResult.toWarningJson(formErrors.map { formError =>
    {
      val errorKey: String =
        if (formError.message.nonEmpty && formError.message != "error.required") {
          formError.message
        } else {
          formError.key
        }

      WarningParameter(
        errorKey,
        "warn." + errorKey,
        "warn." + errorKey)
    }
    })))
  }

  protected def error[T](status: Status, code: Int, message: String): Result =
    status(write(APIResult.toErrorJson(code, message)))

  protected def badRequestWarning(warning: WarningParameter): Result =
    BadRequest(write(APIResult.toWarningJson(Seq(warning))))

}
