package controller.post

import Services.PostServices
import controller.APIController
import controller.authentication.Authenticator
import dto.WarningParameter
import dto.form.PostForm
import dto.post.PostResponse
import dto.token.UserPayload
import exeption.EntityNotFoundException
import org.joda.time.DateTime
import pdi.jwt.JwtSession.RESPONSE_HEADER_NAME
import play.api.Configuration
import play.api.mvc.{Action, AnyContent, ControllerComponents}
import post.model.{Post, PostId}
import user.model.UserId

import javax.inject.Inject
import scala.util.{Failure, Success, Try}

class PostControllers @Inject()(postServices: PostServices, authenticator: Authenticator, cc: ControllerComponents)(implicit conf: Configuration) extends APIController(cc) {

  def findAllPost(): Action[AnyContent] = Action {
    val result = postServices.getAll()
    result match {
      case Success(value) => success(PostResponse(value))
      case _ => error(InternalServerError, 500, "Cannot find any post")
    }
  }

  def findByIdPost(id: Long): Action[AnyContent] = Action {
    val result = for {
      post <- postServices.findById(id)
    } yield post

    result match {
      case Success(result) => success(PostResponse(result))
      case Failure(exception) => exception match {
        case _: EntityNotFoundException =>
          badRequestWarning(WarningParameter("warn.entityNotFound", "warn.entityNotFound", s"Can't found entity with id=$id"))
        case _ => badRequestWarning(WarningParameter("warn.findByIdError", "warn.findByIdError", exception.getMessage))
      }
    }
  }

  def createPost: Action[AnyContent] = authenticator.JwtAuthentication { implicit request =>
    PostForm.form.bindFromRequest().fold(
      formWithError => badRequestWarning(formWithError.errors),
      post => {
        val postCurrent = Post(PostId(0), post.category, post.title, post.content, post.authorName, Some(new DateTime()), Some(new DateTime()), post.thumbnail, UserId(post.idUser))
        val id = postServices.create(postCurrent)
        id match {
          case Success(value) => success(value)
          case _ => error(InternalServerError, 500, "Error! Can not create post!")
        }
      }
    )
  }

  def updatePost(): Action[AnyContent] = authenticator.JwtAuthentication { implicit request =>
    PostForm.form.bindFromRequest().fold(
      formWithError => badRequestWarning(formWithError.errors),
      post => {
        val postCurrent = Post(post.identifier, post.category, post.title, post.content, post.authorName, post.createdDate, Some(new DateTime()), post.thumbnail, UserId(post.idUser))
        val id = postServices.update(postCurrent)
        id match {
          case Success(()) => success("Oke")
          case _ => error(InternalServerError, 500, "Error! Can not create post!")
        }
      }
    )
  }

  def deletePost(idPost: Long): Action[AnyContent] = authenticator.JwtAuthentication {
    implicit request => {
      val result = for {
        token <- Try(request.headers.get(RESPONSE_HEADER_NAME(conf)).get.replaceFirst("Bearer ", ""))
        userPayload <- UserPayload.decode(token)
      } yield userPayload.id
      result match {
        case Success(userID) =>
          val post = postServices.findById(idPost)
          post match {
            case Success(p) => if (p.idUser == userID) success(postServices.delete(idPost)) else error(BadRequest, 400, "You have no access!")
            case _ => error(BadRequest, 400, "Not found!")
          }
        case _ => error(BadRequest, 400, "Not found!")
      }
    }
  }

  def findAllPostByUser(id: Long): Action[AnyContent] = authenticator.JwtAuthentication { implicit request =>
    val result = postServices.findByUserId(id)
    result match {
      case Success(value) => success(PostResponse(value))
      case Failure(_) => error(InternalServerError, 500, "Cannot find any post")
    }
  }
}
