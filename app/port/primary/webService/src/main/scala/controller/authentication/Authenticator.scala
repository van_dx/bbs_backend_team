package controller.authentication

import Services.{AuthenticationService, UserService}
import controller.APIController
import dto.token.UserPayload
import pdi.jwt.JwtSession.RESPONSE_HEADER_NAME
import play.api.Configuration
import play.api.libs.json.Json
import play.api.mvc._
import user.model.User

import javax.inject.Inject
import scala.concurrent.{ExecutionContext, Future}
import scala.util.{Failure, Success, Try}

case class UserRequest[A](user: User, request: Request[A]) extends WrappedRequest(request)

class Authenticator @Inject() (
                                bodyParser: BodyParsers.Default,
                                userService: UserService,
                                authenticationService: AuthenticationService,
                                cc: ControllerComponents)(implicit ec: ExecutionContext, conf: Configuration) extends APIController(cc){

  object JwtAuthentication extends ActionBuilder[UserRequest, AnyContent]{

    override def parser: BodyParser[AnyContent] = bodyParser

    override def invokeBlock[A](request: Request[A], block: UserRequest[A] => Future[Result]): Future[Result] = {
      val result = for {
        token <- Try(request.headers.get(RESPONSE_HEADER_NAME(conf)).get.replaceFirst("Bearer ", ""))
        payload <- authenticationService.validateUserToken(token)
        userPayload <- Try(Json.parse(payload).validate[UserPayload].get)
        user <- userService.findById(userPayload.id) if userPayload.email == user.email
      } yield user

      result match {
        case Success(user) => block(UserRequest(user,request))
        case Failure(_) => Future.successful(error(Unauthorized, 403, "Invalid credential"))
      }
    }

    override protected def executionContext: ExecutionContext = ec
  }
}
