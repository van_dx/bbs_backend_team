package Services

import post.PostRepository
import post.model.Post

import javax.inject.Inject
import scala.util.Try

class PostServices @Inject()(postRepository: PostRepository){

  def getAll() = postRepository.findAllActive()
  def findById(id:Long) = postRepository.findById(id)
  def create(post:Post) = postRepository.createPost(post)
  def update(post: Post): Try[Unit] = postRepository.updatePost(post)
  def delete(id: Long): Try[Long] = postRepository.deletePost(id)

  def findByUserId(id: Long) = postRepository.findByUserId(id)

}
