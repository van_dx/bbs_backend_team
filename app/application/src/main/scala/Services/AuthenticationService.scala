package Services

import common.JwtUtility
import exeption.{BaseException, PasswordNotMatch}
import org.mindrot.jbcrypt.BCrypt
import user.UserRepository
import user.model.User

import javax.inject.{Inject, Singleton}
import scala.util.{Failure, Success, Try}

@Singleton
class AuthenticationService @Inject()(userRepository: UserRepository){

  def verify(currentUser: User): Try[User] = {
    val result = for {
      user <- userRepository.findByEmail(currentUser.email)
    } yield user

    result match {
      case Success(user) => if(BCrypt.checkpw(currentUser.password,user.password)&&user.enable)
          Success(user) else Failure(new PasswordNotMatch("password not match"))
      case Failure(err) => Failure(err)
    }
  }

  def validateUserToken(token: String): Try[String] = {
    if(JwtUtility.isValidToken(token)){
      Try(JwtUtility.decodePayload(token).get)
    }else{
      Failure(new BaseException("token is invalid"))
    }
  }
}
