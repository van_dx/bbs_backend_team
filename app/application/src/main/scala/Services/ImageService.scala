package Services

import play.api.libs.Files
import play.api.mvc.MultipartFormData

import java.nio.file.Path
import scala.util.Try

trait ImageService {
  def load(imageName: String): Try[(Path, String)]

  def upload(image: MultipartFormData.FilePart[Files.TemporaryFile]): Try[(Path, String)]
}
