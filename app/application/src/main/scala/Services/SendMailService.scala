package Services

import play.api.libs.mailer.{Email, MailerClient}

import javax.inject.Inject

class SendMailService @Inject()(mailerClient: MailerClient) {

  def  sendEmail(email: String,token: String)  = {
    val  sender  =  Email (
      "Email active account!" ,
      "BBS TEAM <dovanvth@gmail.com>" ,
      Seq (s"""<$email>"""),
      bodyText =  Some("Xin chao"),
      bodyHtml =  Some(
        s"""<html>
           |<head>
           |<style>
           |button{
           |background-image: linear-gradient(135deg, #008aff, #86d472);
           |   border-radius: 6px;
           |   box-sizing: border-box;
           |   color: #ffffff;
           |   display: block;
           |   height: 50px;
           |   font-size: 1.4em;
           |   font-weight: 600;
           |   padding: 4px;
           |   position: relative;
           |   text-decoration: none;
           |   width: 7em;
           |   z-index: 2;
           |}
           |button:hover {
           |    background: #ffff00 !important;
           |    color: red !important;
           |    border-radius:5px;
           |}
           |
           |.button:visited {
           |    width: fit-content;
           |    padding: 0.5em 1em;
           |    text-align: center;
           |    float: inherit;
           |    margin: 0em auto;
           |    color: #ffffff;
           |    background: #c7c350;
           |    border-radius:5px;
           |}
           |</style>
           |</head>
           |<body>
           |<form action='http://localhost:8080/active/$token'>
           |<button>Click here to activation!</button>
           |</form>
           |</body>
           |</html>""".stripMargin )
    )
    mailerClient.send(sender)
  }
}
