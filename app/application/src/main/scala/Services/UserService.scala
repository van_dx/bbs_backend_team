package Services

import user.UserRepository
import user.model.{User, UserId, UserImpl}

import javax.inject.{Inject, Singleton}
import scala.util.Try

@Singleton
class UserService @Inject()(userRepository: UserRepository) {
  def findByEmail(email: String):Try[User] = userRepository.findByEmail(email)
  def update(newUser: UserImpl): Try[Unit] = userRepository.update(newUser)
  def create(newUser: UserImpl):Try[Long] = userRepository.saveNewUser(newUser)
  def findById(id: Long): Try[User] = userRepository.resolveBy(UserId(id))
}
