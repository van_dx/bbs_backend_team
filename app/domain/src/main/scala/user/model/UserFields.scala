package user.model

import modelutil.Fields
import org.joda.time.DateTime

trait UserFields extends Fields{
  val email: String
  val password: String
  val username: String
  val enable: Boolean
  val createdDate: Option[DateTime]
  val updatedDate: Option[DateTime]
}
