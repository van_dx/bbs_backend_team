package user.model

import modelutil.Identifier


case class UserId(value: Long) extends Identifier[Long]