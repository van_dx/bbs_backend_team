package user.model

import modelutil.Entity
import org.joda.time.DateTime


trait User extends Entity[UserId] with UserFields

object User{
  def apply(
           id: UserId,
           email: String,
           password: String,
           username: String,
           enable: Boolean,
           createdDate: Option[DateTime],
           updatedDate:Option[DateTime]
           ): User = UserImpl(
    id,
    email,
    password,
    username,
    enable,
    createdDate,
    updatedDate
   )

  def apply(email: String, password: String): User = UserImpl(UserId(0), email, password, "",false,Some(new DateTime()),Some(new DateTime()))
  def apply(email: String, password: String,username:String): User = UserImpl(UserId(0), email, password, username,false,Some(new DateTime()),Some(new DateTime()))
  def apply(email: String, password: String, username: String, enable: Boolean): User = UserImpl(UserId(0), email, password, username, enable,Some(new DateTime()),Some(new DateTime()))
  def apply(user: User): UserImpl = UserImpl(user.identifier, user.email, user.password, user.username, true,user.createdDate,user.updatedDate)

  def unapply(arg: User): Option[Nothing] = None
}
