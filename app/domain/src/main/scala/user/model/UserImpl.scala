package user.model

import org.joda.time.DateTime

case class UserImpl(
                     identifier: UserId = UserId(0),
                     email: String,
                     password: String,
                     username: String,
                     enable: Boolean,
                     override val createdDate: Option[DateTime],
                     override val updatedDate: Option[DateTime],
                   ) extends User {
}