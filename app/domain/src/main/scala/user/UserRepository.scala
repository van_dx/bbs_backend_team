package user

import repository.FeatureWithIdFieldRepository
import user.model.{User, UserFields, UserId, UserImpl}

import scala.util.Try

trait UserRepository extends FeatureWithIdFieldRepository[UserId, UserFields, User]{
  def findByEmail(email: String): Try[User]
  def saveNewUser(newUser: UserImpl): Try[Long]
}
