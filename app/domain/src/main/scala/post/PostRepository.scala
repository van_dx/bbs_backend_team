package post

import post.model._
import repository.FeatureWithIdFieldRepository

import scala.util.Try

trait PostRepository extends FeatureWithIdFieldRepository[PostId,PostFields,Post]{
  def findById(id: Long): Try[Post]
  def getNumOfPosts: Try[Int]
  def findAllActive(): Try[Seq[Post]]
  def createPost(post: Post): Try[Long]
  def updatePost(post: Post): Try[Unit]
  def deletePost(id:Long) : Try[Long]
  def findByName(value: String) : Try[Seq[Post]]
  def findByUserId(id: Long): Try[Seq[Post]]
}
