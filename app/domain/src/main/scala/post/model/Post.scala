package post.model

import modelutil.Entity
import org.joda.time.DateTime
import user.model.{UserId}

trait Post extends Entity[PostId] with PostFields

object Post {
  def apply(
             identifier: PostId,
             category: String,
             title: String,
             content: String,
             authorName: String,
             createdDate: Option[DateTime],
             updatedDate: Option[DateTime],
             thumbnail: String,
             idUser: UserId
           ): Post = PostImpl(
    identifier,
    category,
    title,
    content,
    authorName,
    createdDate,
    updatedDate,
    thumbnail,
    idUser.value
  )


  def unapply(arg: Post): Option[Nothing] = None
}
