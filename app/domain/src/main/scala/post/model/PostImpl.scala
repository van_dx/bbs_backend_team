package post.model

import org.joda.time.DateTime

case class PostImpl(
                     identifier: PostId = PostId(0),
                     category: String,
                     title: String,
                     content: String,
                     authorName: String,
                     override val createdDate: Option[DateTime],
                     override val updatedDate: Option[DateTime],
                     thumbnail: String,
                     idUser: Long
                   ) extends Post