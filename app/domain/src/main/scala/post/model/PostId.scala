package post.model

import modelutil.Identifier

case class PostId(value: Long) extends Identifier[Long]
