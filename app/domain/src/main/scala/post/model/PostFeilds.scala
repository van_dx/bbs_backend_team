package post.model

import modelutil.Fields
import org.joda.time.DateTime
import user.model.UserId

trait PostFields extends Fields{
  val category: String
  val title: String
  val content: String
  val authorName: String
  val createdDate: Option[DateTime]
  val updatedDate: Option[DateTime]
  val thumbnail: String
  val idUser: Long
}
