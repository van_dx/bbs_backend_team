package common

import pdi.jwt.{Jwt, JwtAlgorithm, JwtClaim, JwtHeader, JwtJson, JwtOptions}

import java.time.Clock
import scala.util.Try

object JwtUtility {
  val EXPIRE_TIME = 864000 * 30
  val algorithm = JwtAlgorithm.HS256
  val secretKey = "123456789"
  implicit val clock: Clock = Clock.systemUTC

  def generateToken(payload: String): String = {
    val header = JwtHeader(algorithm)
    val claimSet = JwtClaim(payload).expiresIn(EXPIRE_TIME)
    JwtJson.encode(header, claimSet, secretKey)
  }

  def isValidToken(jwtToken: String): Boolean = {
    Jwt.isValid(jwtToken, secretKey, Seq(algorithm), JwtOptions(leeway = EXPIRE_TIME))
  }

  def decodePayload(jwtToken: String): Try[String] = {
    Jwt.decodeRaw(jwtToken, secretKey, Seq(algorithm))
  }
}
