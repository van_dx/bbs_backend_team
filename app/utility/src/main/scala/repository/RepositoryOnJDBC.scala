package repository

import modelutil.Fields

import scala.util.Try

trait Repository[Identifier <: modelutil.Identifier[_], Field <: Fields, Entity <: modelutil.Entity[Identifier]]

trait ResolveFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity] {
  def resolveBy(id: Identifier)(implicit ctx: IOContext = IOContext): Try[Entity]
}

trait ResolveAllFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity] {
  def resolveAll()(implicit ctx: IOContext = IOContext): Try[Seq[Entity]]
}

trait   StoreWithIdFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity] {
  def store(fields: Fields)(implicit ctx: IOContext = IOContext): Try[Identifier]
}

trait StoreNoIdFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity] {
  def store(fields: Fields)(implicit ctx: IOContext = IOContext): Try[Unit]
}
trait UpdateFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity] {
  def update(entity: Entity)(implicit ctx: IOContext = IOContext): Try[Unit]
}
trait DeleteFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends  Repository[Identifier, Fields, Entity]{
  def delete(id: Identifier)(implicit  ctx: IOContext = IOContext): Try[Unit]
}

trait BasicFeatureRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends Repository[Identifier, Fields, Entity]
    with ResolveAllFeatureRepository[Identifier, Fields, Entity]

trait FeatureWithIdFieldRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends BasicFeatureRepository[Identifier, Fields, Entity]
    with ResolveFeatureRepository[Identifier, Fields, Entity]
    with StoreWithIdFeatureRepository[Identifier, Fields, Entity]
    with UpdateFeatureRepository[Identifier,Fields,Entity]
    with DeleteFeatureRepository[Identifier,Fields,Entity]
trait FeatureNoIdFieldRepository[Identifier <: modelutil.Identifier[_], Fields <: modelutil.Fields, Entity <: modelutil.Entity[Identifier]]
  extends BasicFeatureRepository[Identifier, Fields, Entity]
    with StoreNoIdFeatureRepository[Identifier, Fields, Entity]