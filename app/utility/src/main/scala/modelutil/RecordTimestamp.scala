package modelutil

import org.joda.time.DateTime

trait RecordTimestamp {
  val createdDate: Option[DateTime] = None
  val updatedDate: Option[DateTime] = None
}