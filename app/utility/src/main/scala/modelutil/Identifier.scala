package modelutil

trait Identifier[+A] extends Serializable {
  def value: A
}
