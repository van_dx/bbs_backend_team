# --- !Ups
CREATE TABLE post (
                      id bigint(20) NOT NULL AUTO_INCREMENT,
                      category varchar(255) NOT NULL,
                      title varchar(255) NOT NULL,
                      content text NOT NULL,
                      author_name varchar (255) NOT NULL,
                      created_date DATETIME NOT NULL,
                      updated_date DATETIME NOT NULL,
                      thumbnail varchar (255) NOT NULL,
                      id_user bigint(20) NOT NULL ,
                      PRIMARY KEY (id));
# --- !Downs
DROP TABLE post;


