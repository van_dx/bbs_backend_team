# --- !Ups
CREATE TABLE users (
                       id bigint(20) NOT NULL AUTO_INCREMENT,
                       email varchar(255) NOT NULL,
                       password varchar(255) NOT NULL,
                       username varchar(100) NOT NULL,
                       enable boolean default false,
                       created_date DATETIME NOT NULL,
                       updated_date DATETIME NOT NULL,
                       PRIMARY KEY (id));
# --- !Downs
DROP TABLE users;