package module

import Services.ImageService
import com.google.inject.AbstractModule
import file.ImageHandle
import post.{PostRepository, PostRepositoryOnJDBCImpl}
import user.{UserRepository, UserRepositoryOnJDBCImpl}


class BBSModule extends AbstractModule {
  lazy val postRepository: PostRepository = new PostRepositoryOnJDBCImpl
  lazy val userRepository: UserRepository = new UserRepositoryOnJDBCImpl
  lazy val imageService: ImageService = new ImageHandle

  override def configure(): Unit = {
    bind(classOf[PostRepository]).toInstance(postRepository)
    bind(classOf[UserRepository]).toInstance(userRepository)
    bind(classOf[ImageService]).toInstance(imageService)
  }
}