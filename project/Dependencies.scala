import play.sbt.PlayImport.evolutions
import sbt._


object Dependencies {
  val mysqlConnectorJava = "mysql" % "mysql-connector-java" % "8.0.22"
  lazy val scalikejdbcVersion = "3.5.0"
  val scalikejdbc = "org.scalikejdbc" %% "scalikejdbc" % scalikejdbcVersion
  val scalikejdbcTest = "org.scalikejdbc" %% "scalikejdbc-test" % scalikejdbcVersion % Test
  val scalikejdbcConfig = "org.scalikejdbc" %% "scalikejdbc-config" % scalikejdbcVersion
  val scalikejdbcPlayInit = "org.scalikejdbc" %% "scalikejdbc-play-initializer" % "2.8.0-scalikejdbc-3.5"

  val skinnyOrm = "org.skinny-framework" %% "skinny-orm" % "3.0.3"

  val scalaTestPlus = "org.scalatestplus.play" %% "scalatestplus-play" % "5.0.0" % Test

  val scalaGuice = "net.codingwell" %% "scala-guice" % "4.2.6"

  val json4s = "org.json4s" %% "json4s-jackson" % "3.6.7"

  val tika = "org.apache.tika" % "tika-core" % "1.22"

  val jodaTime = "joda-time" % "joda-time" % "2.10.3"

  val logger = "org.slf4j" % "slf4j-api" % "1.7.25"

  val makeCsv = "au.com.bytecode" % "opencsv" % "2.4"

  val jwtPlay = "com.pauldijou" %% "jwt-play" % "4.0.0"
  val jwtCore = "com.pauldijou" %% "jwt-core" % "4.0.0"
  val jwtRsa = "com.auth0" % "jwks-rsa" % "0.6.1"
  val bcrypt = "com.github.t3hnar" %% "scala-bcrypt" % "4.1"
  val mail = "com.typesafe.play" %% "play-mailer" % "8.0.1"
  val mailguide = "com.typesafe.play" %% "play-mailer-guice" % "8.0.1"
  val portDatabaseDependencies = Seq(
    scalikejdbc,
    scalikejdbcConfig,
    scalikejdbcPlayInit,
    scalikejdbcTest,
    mysqlConnectorJava,
    skinnyOrm,
    evolutions
  )

  val portUtilityDependencies = Seq(
    json4s, logger, jodaTime
  )
}

